﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class BallControllerLV1 : MonoBehaviour {

    public float BallSpeed;

    private Rigidbody rdbody;

    public GameObject BallObject;

    public int Platform;


    void Start()
    {


        rdbody = GetComponent<Rigidbody>();
        Screen.orientation = ScreenOrientation.LandscapeLeft;
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 30;

        if (Application.platform == RuntimePlatform.Android)// Android
        {
            Platform = 0;

        }
        else if (Application.platform != RuntimePlatform.Android) //Not Android
        {
            Platform = 1;

        }

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float MoveHorizontal = 0.0f;
        float MoveVertical = 0.0f;

        if (Platform == 0)
        {

            MoveHorizontal = Input.acceleration.x; //Mobile Accelerometer Input 
            MoveVertical = Input.acceleration.y; //Mobile Accelerometer Input 





            if (Input.touchCount > 0) //Check for touch Inputs too
            {
                MoveHorizontal = Input.touches[0].deltaPosition.x;//MOBILE TOUCH  CONTROL
                MoveVertical = Input.touches[0].deltaPosition.y;//MOBILE TOUCH  CONTROL
            }

        }
        else if (Platform == 1)
        {
            //  MoveHorizontal = Input.GetAxis("Mouse X"); //PC MOUSE  CONTROL
            //  MoveVertical = Input.GetAxis("Mouse Y");//PC MOUSE  CONTROL

            MoveHorizontal = Input.GetAxis("Horizontal"); //PC ARROW CONTROL
            MoveVertical = Input.GetAxis("Vertical"); //PC ARROW CONTROL

        }

        if (BallObject.transform.position.y < -10.0f)
        { // if its out of plane and dropping down
            Debug.Log("Triggered");
         //   BallObject.transform.position = new Vector3(-18.01178f, 1.01747f, 16.22882f); //Reset the ball location
            SceneManager.LoadScene("Level_2");


        }





        Vector3 movement = new Vector3(MoveHorizontal, 0.0f, MoveVertical); // y in the game is the height P.S: 0 CUZ THE BALL WON'T JUMP


        rdbody.AddForce(movement * BallSpeed);

    }
}
