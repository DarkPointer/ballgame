﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {


	public GameObject Ball;

	private Vector3 offset;



	// Use this for initialization
	void Start () {
		offset = transform.position - Ball.transform.position;

	}
	
	void LateUpdate ()
	{
		transform.position = Ball.transform.position + offset;
	}
}
