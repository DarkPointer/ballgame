﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeLevel : MonoBehaviour {

	void OnTriggerEnter(Collider other)
	{

        string lpLoadLevelName = SceneManager.GetActiveScene().name;

     

        string[] splitter = lpLoadLevelName.Split(("_").ToCharArray());

        int curlevel = 0;

        int.TryParse(splitter[1],out curlevel);

        int nextlevel = curlevel+1;

        string lpNextLevel = splitter[0] + "_" + nextlevel.ToString();


     


		SceneManager.LoadScene (lpNextLevel);

	}
}
