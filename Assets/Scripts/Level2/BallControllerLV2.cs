﻿using UnityEngine;
using System.Collections;
using System.Threading;

public class BallControllerLV2 : MonoBehaviour {
    public static BallControllerLV2 instace; //make this accessible from other classes


    public float BallSpeed;

    private Rigidbody rdbody;

    public GameObject BallObject;

    public int Platform;

    public  float MoveHeight = 12.0f;



 



  



    void Start()
    {

      
      //  StartCoroutine(PerSecondLogic());
        instace = this;

        rdbody = GetComponent<Rigidbody>();

        Screen.orientation = ScreenOrientation.LandscapeLeft;
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 30;

        if (Application.platform == RuntimePlatform.Android)// Android
        {
            Platform = 0;

        }
        else if (Application.platform != RuntimePlatform.Android) //Not Android
        {
            Platform = 1;

        }
     
        
        //BallObject = GameObject.FindGameObjectWithTag("Sphere"); find by tag
    }

    // Update is called once per frame
    void FixedUpdate()
    {


        float MoveY = 0.0f;


        float MoveHorizontal = 0.0f;
        float MoveVertical = 0.0f;

        if (Platform == 0)
        {

            MoveHorizontal = Input.acceleration.x; //Mobile Accelerometer Input 
            MoveVertical = Input.acceleration.y; //Mobile Accelerometer Input 





            if (Input.touchCount > 0) //Check for touch Inputs too
            {
                MoveHorizontal = Input.touches[0].deltaPosition.x;//MOBILE TOUCH  CONTROL
                MoveVertical = Input.touches[0].deltaPosition.y;//MOBILE TOUCH  CONTROL
            }

        }
        else if (Platform == 1)
        {
            //  MoveHorizontal = Input.GetAxis("Mouse X"); //PC MOUSE  CONTROL
            //  MoveVertical = Input.GetAxis("Mouse Y");//PC MOUSE  CONTROL

            MoveHorizontal = Input.GetAxis("Horizontal"); //PC ARROW CONTROL
            MoveVertical = Input.GetAxis("Vertical"); //PC ARROW CONTROL

        }

        if (BallObject.transform.position.y < -10.0f)
        { // if its out of plane and dropping down

            BallObject.transform.position = new Vector3(-18.01178f, 1.01747f, 16.22882f); //Reset the ball location
            


        }



        if (BallObject.transform.position.y > 7.8f) //control the gravity so the ball doesn't jump outside
        { 

       //     Debug.Log("gravity added");
            Physics.gravity = new Vector3(0, -50, 0);
        }
        else if (BallObject.transform.position.y < 1.8f) //control the gravity to normal
        {
         //   Debug.Log("gravity restored");
            Physics.gravity = new Vector3(0, -30, 0);
        }




      if(  Input.GetKeyDown(KeyCode.Space) == true && OnGround())
      {
          MoveY += MoveHeight; //jump


      }



        Vector3 movement = new Vector3(MoveHorizontal, MoveY, MoveVertical); 


        rdbody.AddForce(movement * BallSpeed);

    }

    IEnumerator PerSecondLogic()
    {
        while (true)
        {
            OnGround();
            yield return new WaitForSeconds(1);
        }
    }






    bool OnGround()
    {

        if (BallObject.transform.position.y < 1.4f) //check if ball is on ground
        {

            return true;
        }


        return false;


    }

}
