﻿using UnityEngine;
using System.Collections;

public class HeightCollider : MonoBehaviour {


    private GameObject Ball;


    void Start()
    {

        Ball = GameObject.FindGameObjectWithTag("Sphere");//another method to catch the ball
    }

    void OnTriggerEnter(Collider other)
    {

        Material newMat = Resources.Load("Materials/Level_1", typeof(Material)) as Material;
      
        Ball.GetComponent<Renderer>().material = newMat;


        BallControllerLV2 BallController = Ball.GetComponent<BallControllerLV2>();

        BallController.MoveHeight = 50.0f;

    //    BallControllerLV2.instace.MoveHeight = 25.0f; can be used too

        Debug.Log("In-Changing MoveHeight");

     
    }
}
